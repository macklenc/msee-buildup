import dolfin as fem
import matplotlib.pyplot as plt
import mtnlion.formulas.approximation
import mtnlion.formulas.dfn
import mtnlion.tools.helpers
import numpy as np

import mtnlion.domain
from buildup import utilities, buildup_utilities, buildup_equations


@mtnlion.domain.eval_domain("anode", "cathode")
def sum_cs(items):
    return sum(items)


def run(mesh, start_time, dt, stop_time, return_comsol=False):
    time = np.arange(start_time, stop_time + dt, dt)
    dtc = fem.Constant(dt)
    sim, raw_sim = buildup_utilities.preprocessor(
        mesh,
        "GuAndWang_isothermal.xlsx",
        "comsol_solution/isothermal/hifi/guwang_hifi.npz",
        "comsol_solution/isothermal/hifi/input_current.csv.bz2",
        ic_interp="cubic",
    )

    print("Setting up solution space...")

    Ns = 8
    Ne = 2 * Ns

    a_sol = mtnlion.tools.helpers.set_domain_data(
        mtnlion.tools.helpers.create_solution_matrices(len(time), len(raw_sim.mesh), Ns),
        mtnlion.tools.helpers.create_solution_matrices(len(time), len(raw_sim.mesh), Ns),
    )

    # format: anode[0], ..., anode[num_functions], cathode[0], ..., cathode[num_functions]
    ME = fem.MixedElement([sim.P1] * Ne)  # need twice num_functions, one for anode, one for cathode
    W = fem.FunctionSpace(sim.mesh, ME)

    # Using Newton iteration... The du term is now the trial function
    du = fem.TrialFunction(W)
    v = fem.TestFunction(W)
    u = fem.Function(W)
    u_1 = fem.Function(W)  # previous solution

    a_n = fem.split(u)  # list of current solutions
    a_n_1 = fem.split(u_1)  # list of previous solutions
    a_m = fem.split(v)

    # format: anode[0], cathode[0], anode[1], cathode[1],...
    cs_an = mtnlion.tools.helpers.set_domain_data(a_n[0:Ns], a_n[Ns:])
    cs_an_1 = mtnlion.tools.helpers.set_domain_data(a_n_1[0:Ns], a_n_1[Ns:])
    v_am = mtnlion.tools.helpers.set_domain_data(a_m[0:Ns], a_m[Ns:])

    phis_c, phie_c, ce_c = sim.comsol_data.funcs["phis"], sim.comsol_data.funcs["phie"], sim.comsol_data.funcs["ce"]

    cse = sum_cs(cs_an)

    Uocp = buildup_equations.U_ocp(cse, sim.params.csmax, sim.params.Uocp)
    eta = buildup_equations.eta(phis_c, phie_c, Uocp)
    j = buildup_equations.j(
        ce_c,
        cse,
        eta,
        sim.params.csmax,
        sim.consts.ce0,
        sim.params.alpha,
        sim.params.k_norm_ref,
        sim.consts.F,
        sim.consts.R,
        sim.consts.T,
    )

    euler = buildup_equations.euler(cs_an, cs_an_1, dtc)

    print("Performing a priori calculations...")
    legendre = mtnlion.formulas.approximation.Legendre(Ns)
    Ftmp = buildup_equations.c_s(euler, cs_an, v_am, sim.params.Rs, sim.params.Ds_ref, legendre)
    Ftmp_n = buildup_equations.c_s_neumann(j, v_am, sim.params.Rs)

    print("Assembling FEM form...")
    F = Ftmp["anode"] * sim.dx + Ftmp["cathode"] * sim.dx
    F += Ftmp_n["anode"] * sim.dx + Ftmp_n["cathode"] * sim.dx

    print("Loading problem space...")
    J = fem.derivative(F, u, du)
    problem = fem.NonlinearVariationalProblem(F, u, J=J)
    solver = fem.NonlinearVariationalSolver(problem)

    prm = solver.parameters
    prm["newton_solver"]["absolute_tolerance"] = 1e-8
    prm["newton_solver"]["relative_tolerance"] = 1e-7
    prm["newton_solver"]["maximum_iterations"] = 25
    prm["newton_solver"]["relaxation_parameter"] = 1.0

    print("Initializing...")
    cs0 = sim.params.cs_0

    assigner = fem.FunctionAssigner(W, [sim.V] * Ne)
    functions = [fem.project(fem.Constant(0), sim.V) for _ in range(len(u_1))]

    functions[0] = fem.project(cs0["anode"], sim.V)
    functions[Ns] = fem.project(cs0["cathode"], sim.V)

    assigner.assign(u_1, functions)

    u.assign(u_1)
    print("Starting simulation.")
    for k, t in enumerate(time):
        sim.comsol_data.update(t)

        iterations, converged = solver.solve()
        u_1.assign(u)

        for n in range(0, Ns):
            a_sol["anode"][n][k, :] = mtnlion.tools.helpers.get_1d(u.sub(n, True), sim.V)
            a_sol["cathode"][n][k, :] = mtnlion.tools.helpers.get_1d(u.sub(n + Ns, True), sim.V)

        print("t={time:.3f}: num iterations: {iter}".format(time=t, iter=iterations))

    print("Performing ex post facto interpolations...")
    cse_sol = {
        "anode": utilities.interp_time(time, sum(a_sol["anode"])),
        "cathode": utilities.interp_time(time, sum(a_sol["cathode"])),
    }

    a_sol["anode"] = [utilities.interp_time(time, a_n) for a_n in a_sol["anode"]]
    a_sol["cathode"] = [utilities.interp_time(time, a_n) for a_n in a_sol["cathode"]]

    if return_comsol:
        return cse_sol, a_sol, raw_sim
    else:
        return cse_sol, a_sol


def main(start_time=None, dt=None, stop_time=None, plot_time=None, get_test_stats=False):
    # Quiet
    fem.set_log_level(fem.LogLevel.ERROR)

    # Times at which to run solver
    if start_time is None:
        start_time = 0
    if stop_time is None:
        stop_time = 50
    if dt is None:
        dt = 0.1
    if plot_time is None:
        plot_time = np.arange(start_time, stop_time, (stop_time - start_time) / 10)

    mesh = np.linspace(0, 1, 40)
    cse_sol, a_sol, sim = run(mesh, start_time, dt, stop_time, return_comsol=True)

    if not get_test_stats:
        utilities.report(
            sim.mesh, plot_time, cse_sol["anode"](plot_time), sim.comsol_data.cse["anode"](plot_time), "$c_{s,e}^{neg}$"
        )
        utilities.save_plot(__file__, "plots/cse_neg.png")
        plt.show()
        utilities.report(
            sim.mesh,
            plot_time,
            cse_sol["cathode"](plot_time),
            sim.comsol_data.cse["cathode"](plot_time),
            "$c_{s,e}^{pos}$",
        )
        utilities.save_plot(__file__, "plots/cse_pos.png")
        plt.show()
    # else:
    #     data = utilities.generate_test_stats(time, comsol, phis_sol, comsol_phis)
    #
    #     # Separator info is garbage:
    #     for d in data:
    #         d[1, ...] = 0
    #
    #     return data


if __name__ == "__main__":
    main()
