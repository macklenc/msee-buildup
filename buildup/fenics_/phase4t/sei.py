import dolfin as fem
import matplotlib.pyplot as plt
import mtnlion.domain
import mtnlion.element
import mtnlion.formulas.dfn
import mtnlion.formulas.time_integration
import mtnlion.tools.helpers
import numpy as np
from mtnlion.models import sei as newman

import mtnlion.assemblers
import mtnlion.report
import mtnlion.solution
from buildup import buildup_utilities


def run(mesh, start_time, dt, stop_time, return_comsol=False):
    time = np.arange(start_time, stop_time + dt, dt)
    dtc = fem.Constant(dt, name="time_integration")
    sim, raw_sim = buildup_utilities.preprocessor(
        mesh,
        "GuAndWang_SEI.xlsx",
        "comsol_solution/sei/guwang.npz",
        "comsol_solution/sei/input_current.csv.bz2",
        ic_interp="zero",
    )

    print("Initializing Model...")
    Ns = 8

    params_dict = {
        **{k: v for k, v in sim.params.items() if k not in ["Uocp"]},
        **{k: v for k, v in sim.consts.items() if k not in ["kappa_ref"]},
        "Mp": mtnlion.domain.Domain({"anode": 7.3e4, "cathode": 7.3e4}),
        "rho_p": mtnlion.domain.Domain({"anode": 2.1e3, "cathode": 2.1e3}),
        "kappa_p": mtnlion.domain.Domain({"anode": 1, "cathode": 1}),
        "Rsei": mtnlion.domain.Domain({"anode": 0.01, "cathode": 0}),
        "Uref_oc": 0,
    }

    newman2 = newman.SEI(params_dict, Ns=Ns)
    newman2.add_formula(mtnlion.formulas.dfn.SOC())
    newman2.add_formula(mtnlion.formulas.dfn.Uocp(sim.params.Uocp))
    newman2.add_formula(mtnlion.formulas.dfn.KappaRef(sim.consts.kappa_ref))
    newman2.add_formula(mtnlion.formulas.dfn.KappaEff(), mtnlion.formulas.dfn.KappaDEff())
    newman2.set_dt(
        mtnlion.formulas.time_integration.Euler(trial_functions=["ce", "cs", "delta_film", "Q"], delta_t=dtc)
    )
    newman2.hyper_params["Iapp"] = fem.Constant(0, name="Iapp")

    newman2.set_dim_elements(sim.P1)
    newman2.set_boundary_elements(sim.R0)

    print("Setting up solution space...")
    elements = mtnlion.element.ElementSpace(newman2.trial_functions)
    pseudo_1d = mtnlion.assemblers.OneDimensionalAssembler(newman2, sim.mesh, elements, sim.dx, sim.ds)
    F = pseudo_1d.formulate()

    # print("Formulated Model:")
    # print(pseudo_1d)

    solutions = mtnlion.solution.Solution(pseudo_1d, ["phis", "phie", "ce", "j", "cse", "js"], sim.V)
    solutions.set_solution_time_steps(len(time))

    print("Loading problem space...")
    J = fem.derivative(F, pseudo_1d.u_data_vec[0], pseudo_1d.trial)
    problem_solver = fem.NonlinearVariationalProblem(F, pseudo_1d.u_data_vec[0], J=J)
    solver = fem.NonlinearVariationalSolver(problem_solver)

    prm = solver.parameters
    prm["newton_solver"]["absolute_tolerance"] = 1e-8
    prm["newton_solver"]["relative_tolerance"] = 1e-7
    prm["newton_solver"]["maximum_iterations"] = 100
    prm["newton_solver"]["relaxation_parameter"] = 1.0

    print("Initializing...")
    phis_init = mtnlion.formulas.dfn.Uocp(sim.params.Uocp)(raw_sim.params.theta100)
    phis_init = phis_init["cathode"] - phis_init["anode"]

    init = {
        "cs": mtnlion.tools.helpers.set_domain_data(
            [
                fem.project(
                    sim.params.cs_0["anode"],
                    pseudo_1d.function_subspace[pseudo_1d.element_space.mapping["cs"]["anode"][0]],
                )
            ],
            [
                fem.project(
                    sim.params.cs_0["cathode"],
                    pseudo_1d.function_subspace[pseudo_1d.element_space.mapping["cs"]["cathode"][0]],
                )
            ],
        ),
        "phis": mtnlion.tools.helpers.set_domain_data(
            fem.project(fem.Constant(0), pseudo_1d.function_subspace[pseudo_1d.element_space.mapping["phis"]["anode"]]),
            fem.project(phis_init, pseudo_1d.function_subspace[pseudo_1d.element_space.mapping["phis"]["cathode"]]),
        ),
        "phie": mtnlion.tools.helpers.set_domain_data(
            fem.project(
                -mtnlion.formulas.dfn.Uocp(sim.params.Uocp)(raw_sim.params.theta100)["anode"],
                pseudo_1d.function_subspace[pseudo_1d.element_space.mapping["phie"]["anode"]],
            )
        ),
        "ce": mtnlion.tools.helpers.set_domain_data(
            fem.interpolate(
                sim.consts.ce0, pseudo_1d.function_subspace[pseudo_1d.element_space.mapping["ce"]["anode"]]
            ),
            fem.interpolate(
                sim.consts.ce0, pseudo_1d.function_subspace[pseudo_1d.element_space.mapping["ce"]["cathode"]]
            ),
            fem.interpolate(
                sim.consts.ce0, pseudo_1d.function_subspace[pseudo_1d.element_space.mapping["ce"]["separator"]]
            ),
        ),
    }

    pseudo_1d.assign(pseudo_1d.u_data_vec[1], init)
    pseudo_1d.u_data_vec[0].assign(pseudo_1d.u_data_vec[1])

    print("Starting simulation.")
    for k, t in enumerate(time):
        newman2.hyper_params["Iapp"].assign(sim.Iapp(t))

        iterations, converged = solver.solve()
        pseudo_1d.u_data_vec[1].assign(pseudo_1d.u_data_vec[0])

        print("t={time:.3f}: num iterations: {iter}".format(time=t, iter=iterations))

        solutions.save_solution(k, t)

    # solutions = solutions.interp_time(time, solutions.solutions)

    if return_comsol:
        return (solutions, raw_sim)
    else:
        return solutions


def main(start_time=None, dt=None, stop_time=None, plot_time=None, get_test_stats=False):
    fem.set_log_level(fem.LogLevel.ERROR)
    fem.parameters["form_compiler"]["optimize"] = True
    fem.parameters["form_compiler"]["cpp_optimize"] = True
    fem.parameters["form_compiler"]["cpp_optimize_flags"] = "-O3"
    import numpy as np

    # Times at which to run solver
    if start_time is None:
        start_time = 0.01
    if stop_time is None:
        stop_time = 15.01
    if dt is None:
        dt = 0.1
    if plot_time is None:
        plot_time = np.arange(start_time, stop_time, (stop_time - start_time) / 10)

    mesh = np.linspace(0, 1, 20)
    solutions, sim = run(mesh, start_time, dt, stop_time, return_comsol=True)
    report = mtnlion.report.Report(solutions, plot_time, split=False, comsol_data=sim.comsol_data)

    plt.plot(solutions.time, solutions.solutions["phis"]["cathode"][:, -1])
    plt.plot(solutions.time, sim.comsol_data["phis"]["cathode"](solutions.time)[:, -1])
    plt.show()

    report.plot(local_path=__file__, save="plots")
    print(report.report_rmse())


if __name__ == "__main__":
    main()
