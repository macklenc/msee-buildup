import dolfin as fem
import matplotlib.pyplot as plt
import mtnlion.domain
import mtnlion.tools.helpers
import numpy as np

import buildup.buildup_equations
from buildup import utilities, buildup_equations, buildup_utilities


@mtnlion.domain.eval_domain("anode", "cathode")
def project(v, V):
    return fem.project(v, V)


def run(mesh, start_time, dt, stop_time, return_comsol=False):
    time = np.arange(start_time, stop_time + dt, dt)
    dtc = fem.Constant(dt)
    sim, raw_sim = buildup_utilities.preprocessor(
        mesh,
        "GuAndWang_isothermal.xlsx",
        "comsol_solution/isothermal/hifi/guwang_hifi.npz",
        "comsol_solution/isothermal/hifi/input_current.csv.bz2",
        ic_interp="cubic",
    )

    phis_sol = mtnlion.tools.helpers.set_domain_data(
        *mtnlion.tools.helpers.create_solution_matrices(len(time), len(raw_sim.mesh), 2)
    )
    j_sol = mtnlion.tools.helpers.set_domain_data(
        *mtnlion.tools.helpers.create_solution_matrices(len(time), len(raw_sim.mesh), 2)
    )
    phie_sol = mtnlion.tools.helpers.set_domain_data(
        *mtnlion.tools.helpers.create_solution_matrices(len(time), len(raw_sim.mesh), 3)
    )
    ce_sol = mtnlion.tools.helpers.set_domain_data(
        *mtnlion.tools.helpers.create_solution_matrices(len(time), len(raw_sim.mesh), 3)
    )

    # phis_a, phis_c, phie_a, phie_s, phie_c, ce_a, ce_s, ce_c, lm_phis, lm_phie_as, lm_phie_sc, lm_ce_as, lm_ce_sc
    ME = fem.MixedElement(
        [sim.P1, sim.P1, sim.P1, sim.P1, sim.P1, sim.P1, sim.P1, sim.P1, sim.R0, sim.R0, sim.R0, sim.R0, sim.R0]
    )
    W = fem.FunctionSpace(sim.mesh, ME)

    du = fem.TrialFunction(W)
    u = fem.Function(W)
    ce_1_a = fem.Function(sim.V)
    ce_1_s = fem.Function(sim.V)
    ce_1_c = fem.Function(sim.V)

    (
        phis_a,
        phis_c,
        phie_a,
        phie_s,
        phie_c,
        ce_a,
        ce_s,
        ce_c,
        lm_phis,
        lm_phie_as,
        lm_phie_sc,
        lm_ce_as,
        lm_ce_sc,
    ) = fem.split(u)
    (
        psis_a,
        psis_c,
        psie_a,
        psie_s,
        psie_c,
        cet_a,
        cet_s,
        cet_c,
        mu_phis,
        mu_phie_as,
        mu_phie_sc,
        mu_ce_as,
        mu_ce_sc,
    ) = fem.TestFunction(W)

    phis = mtnlion.tools.helpers.set_domain_data(phis_a, phis_c)
    phie = mtnlion.tools.helpers.set_domain_data(phie_a, phie_c, phie_s)
    ce = mtnlion.tools.helpers.set_domain_data(ce_a, ce_c, ce_s)
    ce_1 = mtnlion.tools.helpers.set_domain_data(ce_1_a, ce_1_c, ce_1_s)

    psis = mtnlion.tools.helpers.set_domain_data(psis_a, psis_c)
    psie = mtnlion.tools.helpers.set_domain_data(psie_a, psie_c, psie_s)
    cet = mtnlion.tools.helpers.set_domain_data(cet_a, cet_c, cet_s)

    cse_c = sim.comsol_data.funcs["cse"]

    sim.params.kappa_ref = buildup_equations.kappa_ref_f(ce, sim.consts.kappa_ref)
    kappa_eff = buildup.buildup_equations.kappa_eff_f(sim.params.kappa_ref, sim.params.eps_e, sim.params.brug_kappa)
    kappa_Deff = buildup.buildup_equations.kappa_Deff_f(sim.params.kappa_ref, sim.params.eps_e, sim.consts.kappa_D)

    Uocp = buildup_equations.U_ocp(cse_c, sim.params.csmax, sim.params.Uocp)
    eta = buildup_equations.eta(phis, phie, Uocp)
    j = buildup_equations.j(
        ce,
        cse_c,
        eta,
        sim.params.csmax,
        sim.consts.ce0,
        sim.params.alpha,
        sim.params.k_norm_ref,
        sim.consts.F,
        sim.consts.R,
        sim.consts.T,
    )

    # ####### Phis
    Iapp = fem.Constant(0)
    Fphis = (
        buildup_equations.phi_s(j, phis, psis, sim.params.a_s, sim.consts.F, sim.params.sigma_eff, sim.params.L)[
            "anode"
        ]
        * sim.dx
        + Iapp / sim.consts.Acell * psis_a * sim.ds(0)
        + buildup_equations.phi_s(j, phis, psis, sim.params.a_s, sim.consts.F, sim.params.sigma_eff, sim.params.L)[
            "cathode"
        ]
        * sim.dx
        + Iapp / sim.consts.Acell * psis_c * sim.ds(1)
    )

    Fphis += lm_phis * psis_a * sim.ds(0) - mu_phis * phis_a * sim.ds(0)

    # ######## Phie
    Ftmp = buildup_equations.phi_e(j, ce, phie, psie, kappa_eff, kappa_Deff, sim.params.L, sim.params.a_s, sim.consts.F)

    Fphie = Ftmp["anode"] * sim.dx + Ftmp["separator"] * sim.dx + Ftmp["cathode"] * sim.dx
    Fphie += lm_phie_as * psie_a * sim.ds(1) - lm_phie_as * psie_s * sim.ds(0)
    Fphie += lm_phie_sc * psie_s * sim.ds(1) - lm_phie_sc * psie_c * sim.ds(0)

    Fphie += phie_a * mu_phie_as * sim.ds(1) - phie_s * mu_phie_as * sim.ds(0)
    Fphie += phie_s * mu_phie_sc * sim.ds(1) - phie_c * mu_phie_sc * sim.ds(0)

    # ######## ce
    euler = buildup_equations.euler(ce, ce_1, dtc)
    # time_integration, jbar, ce, v, a_s, De_eff, t_plus, L, eps_e, domain
    Ftmp = buildup_equations.c_e(
        euler, j, ce, cet, sim.params.a_s, sim.params.De_eff, sim.consts.t_plus, sim.params.L, sim.params.eps_e
    )

    Fce = Ftmp["anode"] * sim.dx + Ftmp["separator"] * sim.dx + Ftmp["cathode"] * sim.dx
    Fce += lm_ce_as * cet_a * sim.ds(1)
    Fce += -lm_ce_sc * cet_s * sim.ds(1) - lm_ce_as * cet_s * sim.ds(0)
    Fce += lm_ce_sc * cet_c * sim.ds(0)

    Fce += mu_ce_as * ce_a * sim.ds(1) - mu_ce_as * ce_s * sim.ds(0)
    Fce += mu_ce_sc * ce_s * sim.ds(1) - mu_ce_sc * ce_c * sim.ds(0)

    # ######## F

    F = Fphis + Fphie + Fce

    fem.assemble(fem.lhs(F))

    J = fem.derivative(F, u, du)
    problem = fem.NonlinearVariationalProblem(F, u, J=J)
    solver = fem.NonlinearVariationalSolver(problem)

    prm = solver.parameters
    prm["newton_solver"]["absolute_tolerance"] = 1e-8
    prm["newton_solver"]["relative_tolerance"] = 1e-7
    prm["newton_solver"]["maximum_iterations"] = 25
    prm["newton_solver"]["relaxation_parameter"] = 1.0

    sim.comsol_data.update(0)

    fem.assign(u.sub(5), fem.interpolate(sim.consts.ce0, sim.V))
    fem.assign(u.sub(6), fem.interpolate(sim.consts.ce0, sim.V))
    fem.assign(u.sub(7), fem.interpolate(sim.consts.ce0, sim.V))
    ce_1["anode"].assign(fem.interpolate(sim.consts.ce0, sim.V))
    ce_1["separator"].assign(fem.interpolate(sim.consts.ce0, sim.V))
    ce_1["cathode"].assign(fem.interpolate(sim.consts.ce0, sim.V))

    for k, t in enumerate(time):
        fem.assign(u.sub(0), fem.project(Uocp["anode"], sim.V))
        fem.assign(u.sub(1), fem.project(Uocp["cathode"], sim.V))

        Iapp.assign(sim.Iapp(t))

        if t > 0:
            sim.comsol_data.update(t - dt)

        iterations, converged = solver.solve()

        print("t={time:.3f}: num iterations: {iter}".format(time=t, iter=iterations))

        fem.assign(ce_1["anode"], u.sub(5))
        fem.assign(ce_1["separator"], u.sub(6))
        fem.assign(ce_1["cathode"], u.sub(7))

        j_sol["anode"][k, :] = mtnlion.tools.helpers.get_1d(fem.project(j["anode"], sim.V), sim.V)
        j_sol["cathode"][k, :] = mtnlion.tools.helpers.get_1d(fem.project(j["cathode"], sim.V), sim.V)

        phis_sol["anode"][k, :] = mtnlion.tools.helpers.get_1d(u.sub(0, True), sim.V)
        phis_sol["cathode"][k, :] = mtnlion.tools.helpers.get_1d(u.sub(1, True), sim.V)

        phie_sol["anode"][k, :] = mtnlion.tools.helpers.get_1d(u.sub(2, True), sim.V)
        phie_sol["separator"][k, :] = mtnlion.tools.helpers.get_1d(u.sub(3, True), sim.V)
        phie_sol["cathode"][k, :] = mtnlion.tools.helpers.get_1d(u.sub(4, True), sim.V)

        ce_sol["anode"][k, :] = mtnlion.tools.helpers.get_1d(u.sub(5, True), sim.V)
        ce_sol["separator"][k, :] = mtnlion.tools.helpers.get_1d(u.sub(6, True), sim.V)
        ce_sol["cathode"][k, :] = mtnlion.tools.helpers.get_1d(u.sub(7, True), sim.V)

    j_sol["anode"] = utilities.interp_time(time, j_sol["anode"])
    j_sol["cathode"] = utilities.interp_time(time, j_sol["cathode"])

    phis_sol["anode"] = utilities.interp_time(time, phis_sol["anode"])
    phis_sol["cathode"] = utilities.interp_time(time, phis_sol["cathode"])

    phie_sol["anode"] = utilities.interp_time(time, phie_sol["anode"])
    phie_sol["separator"] = utilities.interp_time(time, phie_sol["separator"])
    phie_sol["cathode"] = utilities.interp_time(time, phie_sol["cathode"])

    ce_sol["anode"] = utilities.interp_time(time, ce_sol["anode"])
    ce_sol["separator"] = utilities.interp_time(time, ce_sol["separator"])
    ce_sol["cathode"] = utilities.interp_time(time, ce_sol["cathode"])

    if return_comsol:
        return (phis_sol, phie_sol, ce_sol, j_sol, raw_sim)
    else:
        return (phis_sol, phie_sol, ce_sol, j_sol)


def main(start_time=None, dt=None, stop_time=None, plot_time=None, get_test_stats=False):
    fem.set_log_level(fem.LogLevel.ERROR)
    import numpy as np

    # Times at which to run solver
    if start_time is None:
        start_time = 0
    if stop_time is None:
        stop_time = 50
    if dt is None:
        dt = 0.1
    if plot_time is None:
        plot_time = np.arange(start_time, stop_time, (stop_time - start_time) / 10)

    mesh = np.linspace(0, 1, 20)
    phis_sol, phie_sol, ce_sol, j_sol, sim = run(mesh, start_time, dt, stop_time, return_comsol=True)

    # Phis
    utilities.report(
        sim.mesh, plot_time, phis_sol["anode"](plot_time), sim.comsol_data.phis["anode"](plot_time), r"$\Phi_s^{neg}$"
    )
    utilities.save_plot(__file__, "plots/phis_neg.png")
    plt.show()
    utilities.report(
        sim.mesh,
        plot_time,
        phis_sol["cathode"](plot_time),
        sim.comsol_data.phis["cathode"](plot_time),
        r"$\Phi_s^{pos}$",
    )
    utilities.save_plot(__file__, "plots/phis_pos.png")
    plt.show()

    # Phie
    utilities.report(
        sim.mesh, plot_time, phie_sol["anode"](plot_time), sim.comsol_data.phie["anode"](plot_time), r"$\Phi_e^{neg}$"
    )
    utilities.save_plot(__file__, "plots/ce_neg.png")
    plt.show()
    utilities.report(
        sim.mesh,
        plot_time,
        phie_sol["separator"](plot_time),
        sim.comsol_data.phie["separator"](plot_time),
        r"$\Phi_e^{sep}$",
    )
    utilities.save_plot(__file__, "plots/phie_sep.png")
    plt.show()
    utilities.report(
        sim.mesh,
        plot_time,
        phie_sol["cathode"](plot_time),
        sim.comsol_data.phie["cathode"](plot_time),
        r"$\Phi_e^{pos}$",
    )
    utilities.save_plot(__file__, "plots/phie_pos.png")
    plt.show()

    # ce
    utilities.report(
        sim.mesh, plot_time, ce_sol["anode"](plot_time), sim.comsol_data.ce["anode"](plot_time), "$c_e^{neg}$"
    )
    utilities.save_plot(__file__, "plots/ce_neg.png")
    plt.show()
    utilities.report(
        sim.mesh, plot_time, ce_sol["separator"](plot_time), sim.comsol_data.ce["separator"](plot_time), "$c_e^{sep}$"
    )
    utilities.save_plot(__file__, "plots/ce_sep.png")
    plt.show()
    utilities.report(
        sim.mesh, plot_time, ce_sol["cathode"](plot_time), sim.comsol_data.ce["cathode"](plot_time), "$c_e^{pos}$"
    )
    utilities.save_plot(__file__, "plots/ce_pos.png")
    plt.show()

    # j
    utilities.report(sim.mesh, plot_time, j_sol["anode"](plot_time), sim.comsol_data.j["anode"](plot_time), "$j_{neg}$")
    utilities.save_plot(__file__, "plots/j_neg.png")
    plt.show()
    utilities.report(
        sim.mesh, plot_time, j_sol["cathode"](plot_time), sim.comsol_data.j["cathode"](plot_time), "$j_{pos}$"
    )
    utilities.save_plot(__file__, "plots/j_pos.png")

    plt.show()


if __name__ == "__main__":
    main()
