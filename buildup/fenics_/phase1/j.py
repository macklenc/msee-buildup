import sys

import dolfin as fem
import matplotlib.pyplot as plt
import mtnlion.tools.helpers
import numpy as np

import mtnlion.domain
from buildup import utilities, buildup_utilities, buildup_equations


@mtnlion.domain.eval_domain("anode", "cathode")
def project(v, V):
    return fem.project(v, V)


def run(time, return_comsol=False, form="equation", mesh=None):
    sim, raw_sim = buildup_utilities.preprocessor(
        mesh,
        "GuAndWang_isothermal.xlsx",
        "comsol_solution/isothermal/hifi/guwang_hifi.npz",
        "comsol_solution/isothermal/hifi/input_current.csv.bz2",
        ic_interp="cubic",
    )

    j_sol = mtnlion.tools.helpers.set_domain_data(
        *mtnlion.tools.helpers.create_solution_matrices(len(time), len(raw_sim.mesh), 2)
    )

    phis_c, phie_c, cse_c, ce_c = (
        sim.comsol_data.funcs["phis"],
        sim.comsol_data.funcs["phie"],
        sim.comsol_data.funcs["cse"],
        sim.comsol_data.funcs["ce"],
    )

    if form == "equation":
        Uocp = buildup_equations.U_ocp(sim.comsol_data.funcs["cse"], sim.params.csmax, sim.params.Uocp)
        sim.params.Uocp = Uocp
    elif form == "interp":
        sim.params.Uocp = buildup_equations.Uocp_interp(sim.Uocp_spline, cse_c, sim.params.csmax)
    else:
        return

    # ce, cse, phie, phis, Uocp, csmax, ce0, alpha, k_norm_ref, F, R, T
    eta = buildup_equations.eta(phis_c, phie_c, sim.params.Uocp)
    jbar = buildup_equations.j(
        ce_c,
        cse_c,
        eta,
        sim.params.csmax,
        sim.consts.ce0,
        sim.params.alpha,
        sim.params.k_norm_ref,
        sim.consts.F,
        sim.consts.R,
        sim.consts.T,
    )

    for k, t in enumerate(time):
        sim.comsol_data.update(t)

        asdf = project(jbar, sim.V)
        j_sol["anode"][k, :] = mtnlion.tools.helpers.get_1d(asdf["anode"], sim.V)
        j_sol["cathode"][k, :] = mtnlion.tools.helpers.get_1d(asdf["cathode"], sim.V)

    j_sol["anode"] = utilities.interp_time(time, j_sol["anode"])
    j_sol["cathode"] = utilities.interp_time(time, j_sol["cathode"])

    if return_comsol:
        return j_sol, raw_sim
    else:
        return j_sol


def main(time=None, plot_time=None, get_test_stats=False, mesh=None):
    # Quiet
    fem.set_log_level(fem.LogLevel.ERROR)
    mesh = np.linspace(0, 1, 100)

    # Times at which to run solver
    if time is None:
        time = np.arange(0, 50, 5)
    if plot_time is None:
        plot_time = time

    j_sol, sim = run(time, return_comsol=True, form="interp", mesh=mesh)

    if not get_test_stats:
        utilities.report(sim.mesh, time, j_sol["anode"](plot_time), sim.comsol_data.j["anode"](plot_time), "$j_{neg}$")
        utilities.save_plot(__file__, "plots/compare_j_neg.png")
        plt.show()
        utilities.report(
            sim.mesh, time, j_sol["cathode"](plot_time), sim.comsol_data.j["cathode"](plot_time), "$j_{pos}$"
        )
        utilities.save_plot(__file__, "plots/comsol_compare_j_pos.png")

        plt.show()
    # else:
    # data = utilities.generate_test_stats(time, comsol, j_sol, comsol_j)

    # Separator info is garbage:
    # for d in data:
    #     d[1, ...] = 0
    #
    # return data

    return 0


if __name__ == "__main__":
    sys.exit(main())
