import dolfin as fem
import matplotlib.pyplot as plt
import mtnlion.tools.helpers

import buildup.buildup_equations
from buildup import utilities, buildup_utilities, buildup_equations


def run(mesh, time, dt, return_comsol=False):
    sim, raw_sim = buildup_utilities.preprocessor(
        mesh,
        "GuAndWang_isothermal.xlsx",
        "comsol_solution/isothermal/hifi/guwang_hifi.npz",
        "comsol_solution/isothermal/hifi/input_current.csv.bz2",
        ic_interp="cubic",
    )

    phie_sol = mtnlion.tools.helpers.set_domain_data(
        *mtnlion.tools.helpers.create_solution_matrices(len(time), len(raw_sim.mesh), 3)
    )

    ME = fem.MixedElement([sim.P1, sim.P1, sim.P1, sim.R0, sim.R0])
    W = fem.FunctionSpace(sim.mesh, ME)

    du = fem.TrialFunction(W)
    u = fem.Function(W)

    (phie_a, phie_s, phie_c, lm_as, lm_sc) = fem.split(u)
    (psie_a, psie_s, psie_c, mu_as, mu_sc) = fem.TestFunction(W)

    phie = mtnlion.tools.helpers.set_domain_data(phie_a, phie_c, phie_s)
    v = mtnlion.tools.helpers.set_domain_data(psie_a, psie_c, psie_s)

    phis_c, ce_c, cse_c = sim.comsol_data.funcs["phis"], sim.comsol_data.funcs["ce"], sim.comsol_data.funcs["cse"]

    sim.params.kappa_ref = buildup_equations.kappa_ref_f(ce_c, sim.consts.kappa_ref)
    kappa_eff = buildup.buildup_equations.kappa_eff_f(sim.params.kappa_ref, sim.params.eps_e, sim.params.brug_kappa)
    kappa_Deff = buildup.buildup_equations.kappa_Deff_f(sim.params.kappa_ref, sim.params.eps_e, sim.consts.kappa_D)

    Uocp = buildup_equations.U_ocp(cse_c, sim.params.csmax, sim.params.Uocp)
    eta = buildup_equations.eta(phis_c, phie, Uocp)
    j = buildup_equations.j(
        ce_c,
        cse_c,
        eta,
        sim.params.csmax,
        sim.consts.ce0,
        sim.params.alpha,
        sim.params.k_norm_ref,
        sim.consts.F,
        sim.consts.R,
        sim.consts.T,
    )

    Ftmp = buildup_equations.phi_e(j, ce_c, phie, v, kappa_eff, kappa_Deff, sim.params.L, sim.params.a_s, sim.consts.F)

    F = Ftmp.sum() * sim.dx
    F += lm_as * psie_a * sim.ds(1) - lm_as * psie_s * sim.ds(0)
    F += lm_sc * psie_s * sim.ds(1) - lm_sc * psie_c * sim.ds(0)

    F += phie_a * mu_as * sim.ds(1) - phie_s * mu_as * sim.ds(0)
    F += phie_s * mu_sc * sim.ds(1) - phie_c * mu_sc * sim.ds(0)

    J = fem.derivative(F, u, du)
    problem = fem.NonlinearVariationalProblem(F, u, J=J)
    solver = fem.NonlinearVariationalSolver(problem)

    prm = solver.parameters
    prm["newton_solver"]["absolute_tolerance"] = 1e-8
    prm["newton_solver"]["relative_tolerance"] = 1e-7
    prm["newton_solver"]["maximum_iterations"] = 25
    prm["newton_solver"]["relaxation_parameter"] = 1.0

    for k, t in enumerate(time):
        sim.comsol_data.update(t)

        solver.solve()

        phie_sol[k, :] = mtnlion.tools.helpers.set_domain_data(
            mtnlion.tools.helpers.get_1d(u.sub(0, True), sim.V),
            mtnlion.tools.helpers.get_1d(u.sub(2, True), sim.V),
            mtnlion.tools.helpers.get_1d(u.sub(1, True), sim.V),
        )

    phie_sol = mtnlion.tools.helpers.interp_time2(time, phie_sol)

    if return_comsol:
        return phie_sol, raw_sim
    else:
        return phie_sol


def main(time=None, dt=None, plot_time=None, get_test_stats=False):
    # Quiet
    fem.set_log_level(fem.LogLevel.ERROR)
    import numpy as np

    # Times at which to run solver
    if time is None:
        time = np.arange(0, 50, 5)
    if dt is None:
        dt = 0.1
    if plot_time is None:
        plot_time = time

    mesh = np.linspace(0, 1, 20)
    phie_sol, sim = run(mesh, time, dt, return_comsol=True)

    if not get_test_stats:
        mesh = np.concatenate((sim.mesh, sim.mesh + 1, sim.mesh + 2))
        comsol_phie = np.concatenate(
            (
                sim.comsol_data.phie["anode"](plot_time),
                sim.comsol_data.phie["separator"](plot_time),
                sim.comsol_data.phie["cathode"](plot_time),
            ),
            axis=1,
        )

        utilities.report(
            mesh,
            time,
            np.concatenate(
                (phie_sol["anode"](plot_time), phie_sol["separator"](plot_time), phie_sol["cathode"](plot_time)), 1
            ),
            comsol_phie,
            r"$\Phi_e$",
        )
        utilities.save_plot(__file__, "plots/compare_phie2.png")
        plt.show()

        utilities.report(
            sim.mesh, time, phie_sol["anode"](plot_time), sim.comsol_data.phie["anode"](plot_time), r"$\Phi_e^{neg}$"
        )
        utilities.save_plot(__file__, "plots/compare_phie_neg.png")
        plt.show()

        utilities.report(
            sim.mesh,
            time,
            phie_sol["separator"](plot_time),
            sim.comsol_data.phie["separator"](plot_time),
            r"$\Phi_e^{sep}$",
        )
        utilities.save_plot(__file__, "plots/compare_phie_sep.png")
        plt.show()

        utilities.report(
            sim.mesh,
            time,
            phie_sol["cathode"](plot_time),
            sim.comsol_data.phie["cathode"](plot_time),
            r"$\Phi_e^{pos}$",
        )
        utilities.save_plot(__file__, "plots/compare_phie_pos.png")
        plt.show()
    # else:
    #     data = utilities.generate_test_stats(time, comsol, phie_sol, comsol_phie)
    #
    #     return data


if __name__ == "__main__":
    main()
