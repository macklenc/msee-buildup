import dolfin as fem
import matplotlib.pyplot as plt
import mtnlion.tools.helpers
import numpy as np

from buildup import utilities, buildup_utilities, buildup_equations


def run(mesh, time, dt, return_comsol=False):
    sim, raw_sim = buildup_utilities.preprocessor(
        mesh,
        "GuAndWang_isothermal.xlsx",
        "comsol_solution/isothermal/hifi/guwang_hifi.npz",
        "comsol_solution/isothermal/hifi/input_current.csv.bz2",
        ic_interp="cubic",
    )

    phis_sol = mtnlion.tools.helpers.set_domain_data(
        *mtnlion.tools.helpers.create_solution_matrices(len(time), len(raw_sim.mesh), 2)
    )

    ME = fem.MixedElement([sim.P1, sim.P1, sim.R0])
    W = fem.FunctionSpace(sim.mesh, ME)

    # Using Newton iteration... The du term is now the trial function
    du = fem.TrialFunction(W)
    u = fem.Function(W)

    (phis_a, phis_c, lm) = fem.split(u)
    (psis_a, psis_c, mu) = fem.TestFunction(W)

    phis = mtnlion.tools.helpers.set_domain_data(phis_a, phis_c)
    v = mtnlion.tools.helpers.set_domain_data(psis_a, psis_c)

    phie_c, ce_c, cse_c = sim.comsol_data.funcs["phie"], sim.comsol_data.funcs["ce"], sim.comsol_data.funcs["cse"]

    Uocp = buildup_equations.U_ocp(cse_c, sim.params.csmax, sim.params.Uocp)
    eta = buildup_equations.eta(phis, phie_c, Uocp)
    j = buildup_equations.j(
        ce_c,
        cse_c,
        eta,
        sim.params.csmax,
        sim.consts.ce0,
        sim.params.alpha,
        sim.params.k_norm_ref,
        sim.consts.F,
        sim.consts.R,
        sim.consts.T,
    )

    # jbar, phis, v, a_s, F, sigma_eff, L,
    Iapp = fem.Constant(0)
    F = (
        buildup_equations.phi_s(j, phis, v, sim.params.a_s, sim.consts.F, sim.params.sigma_eff, sim.params.L).sum()
        * sim.dx
    )

    F += Iapp / sim.consts.Acell * psis_a * sim.ds(0) + Iapp / sim.consts.Acell * psis_c * sim.ds(1)
    F += lm * psis_a * sim.ds(0) - mu * phis_a * sim.ds(0)

    J = fem.derivative(F, u, du)
    problem = fem.NonlinearVariationalProblem(F, u, J=J)
    solver = fem.NonlinearVariationalSolver(problem)

    prm = solver.parameters
    prm["newton_solver"]["absolute_tolerance"] = 1e-8
    prm["newton_solver"]["relative_tolerance"] = 1e-7
    prm["newton_solver"]["maximum_iterations"] = 25
    prm["newton_solver"]["relaxation_parameter"] = 1.0

    for k, t in enumerate(time):
        sim.comsol_data.update(t)
        fem.assign(u.sub(0), fem.project(Uocp["anode"], sim.V))
        fem.assign(u.sub(1), fem.project(Uocp["cathode"], sim.V))

        Iapp.assign(sim.Iapp(t))

        solver.solve()

        phis_sol[k, :] = mtnlion.tools.helpers.set_domain_data(
            mtnlion.tools.helpers.get_1d(u.sub(0, True), sim.V), mtnlion.tools.helpers.get_1d(u.sub(1, True), sim.V)
        )

    phis_sol = mtnlion.tools.helpers.interp_time2(time, phis_sol)

    if return_comsol:
        return phis_sol, raw_sim
    else:
        return phis_sol


def main(time=None, dt=None, plot_time=None, get_test_stats=False):
    # Quiet
    fem.set_log_level(fem.LogLevel.ERROR)

    # Times at which to run solver
    if time is None:
        time = np.arange(0, 50, 5)
    if dt is None:
        dt = 0.1
    if plot_time is None:
        plot_time = time

    mesh = np.linspace(0, 1, 50)
    phis_sol, sim = run(mesh, time, dt, return_comsol=True)

    if not get_test_stats:
        utilities.report(
            sim.mesh, time, phis_sol["anode"](plot_time), sim.comsol_data.phis["anode"](plot_time), r"$\Phi_s^{neg}$"
        )
        utilities.save_plot(__file__, "plots/compare_phis_neg_newton.png")
        plt.show()
        utilities.report(
            sim.mesh,
            time,
            phis_sol["cathode"](plot_time),
            sim.comsol_data.phis["cathode"](plot_time),
            r"$\Phi_s^{pos}$",
        )
        utilities.save_plot(__file__, "plots/compare_phis_pos_newton.png")
        plt.show()
    # else:
    #     data = utilities.generate_test_stats(time, comsol, phis_sol, comsol_phis)
    #
    #     # Separator info is garbage:
    #     for d in data:
    #         d[1, ...] = 0
    #
    #     return data


if __name__ == "__main__":
    main()
