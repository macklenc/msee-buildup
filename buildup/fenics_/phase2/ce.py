import dolfin as fem
import matplotlib.pyplot as plt
import mtnlion.tools.helpers
import numpy as np

from buildup import utilities, buildup_utilities, buildup_equations


def run(mesh, time, dt, return_comsol=False):
    dtc = fem.Constant(dt)
    sim, raw_sim = buildup_utilities.preprocessor(
        mesh,
        "GuAndWang_isothermal.xlsx",
        "comsol_solution/isothermal/hifi/guwang_hifi.npz",
        "comsol_solution/isothermal/hifi/input_current.csv.bz2",
        ic_interp="cubic",
    )

    ce_sol = mtnlion.tools.helpers.set_domain_data(
        *mtnlion.tools.helpers.create_solution_matrices(len(time), len(raw_sim.mesh), 3)
    )

    ME = fem.MixedElement([sim.P1, sim.P1, sim.P1, sim.R0, sim.R0])
    W = fem.FunctionSpace(sim.mesh, ME)

    du = fem.TrialFunction(W)
    u = fem.Function(W)

    (ce_a, ce_s, ce_c, lm_as, lm_sc) = fem.split(u)
    (cet_a, cet_s, cet_c, mu_as, mu_sc) = fem.TestFunction(W)

    ce = mtnlion.tools.helpers.set_domain_data(ce_a, ce_c, ce_s)
    v = mtnlion.tools.helpers.set_domain_data(cet_a, cet_c, cet_s)

    phis_c, phie_c, cse_c = sim.comsol_data.funcs["phis"], sim.comsol_data.funcs["phie"], sim.comsol_data.funcs["cse"]
    ce_1 = sim.comsol_data.funcs["ce"]

    Uocp = buildup_equations.U_ocp(cse_c, sim.params.csmax, sim.params.Uocp)
    eta = buildup_equations.eta(phis_c, phie_c, Uocp)
    j = buildup_equations.j(
        ce,
        cse_c,
        eta,
        sim.params.csmax,
        sim.consts.ce0,
        sim.params.alpha,
        sim.params.k_norm_ref,
        sim.consts.F,
        sim.consts.R,
        sim.consts.T,
    )

    euler = buildup_equations.euler(ce, ce_1, dtc)
    # time_integration, jbar, ce, v, a_s, De_eff, t_plus, L, eps_e, domain
    Ftmp = buildup_equations.c_e(
        euler, j, ce, v, sim.params.a_s, sim.params.De_eff, sim.consts.t_plus, sim.params.L, sim.params.eps_e
    )

    F = Ftmp["anode"] * sim.dx + Ftmp["separator"] * sim.dx + Ftmp["cathode"] * sim.dx
    F += lm_as * cet_a * sim.ds(1)
    F += -lm_sc * cet_s * sim.ds(1) - lm_as * cet_s * sim.ds(0)
    F += lm_sc * cet_c * sim.ds(0)

    F += mu_as * ce_a * sim.ds(1) - mu_as * ce_s * sim.ds(0)
    F += mu_sc * ce_s * sim.ds(1) - mu_sc * ce_c * sim.ds(0)

    J = fem.derivative(F, u, du)
    problem = fem.NonlinearVariationalProblem(F, u, J=J)
    solver = fem.NonlinearVariationalSolver(problem)

    prm = solver.parameters
    prm["newton_solver"]["absolute_tolerance"] = 1e-8
    prm["newton_solver"]["relative_tolerance"] = 1e-7
    prm["newton_solver"]["maximum_iterations"] = 25
    prm["newton_solver"]["relaxation_parameter"] = 1.0

    for k, t in enumerate(time):
        sim.comsol_data.update(t - dt)
        fem.assign(u.sub(0), fem.interpolate(sim.consts.ce0, sim.V))
        fem.assign(u.sub(1), fem.interpolate(sim.consts.ce0, sim.V))
        fem.assign(u.sub(2), fem.interpolate(sim.consts.ce0, sim.V))

        solver.solve()

        ce_sol["anode"][k, :] = mtnlion.tools.helpers.get_1d(u.sub(0, True), sim.V)
        ce_sol["separator"][k, :] = mtnlion.tools.helpers.get_1d(u.sub(1, True), sim.V)
        ce_sol["cathode"][k, :] = mtnlion.tools.helpers.get_1d(u.sub(2, True), sim.V)

    ce_sol["anode"] = utilities.interp_time(time, ce_sol["anode"])
    ce_sol["separator"] = utilities.interp_time(time, ce_sol["separator"])
    ce_sol["cathode"] = utilities.interp_time(time, ce_sol["cathode"])

    if return_comsol:
        return ce_sol, raw_sim
    else:
        return ce_sol


def main(time=None, dt=None, plot_time=None, get_test_stats=False):
    # Quiet
    fem.set_log_level(fem.LogLevel.ERROR)

    # Times at which to run solver
    if time is None:
        time = np.arange(1, 50, 5)
    if dt is None:
        dt = 0.1
    if plot_time is None:
        plot_time = time

    mesh = np.linspace(0, 1, 20)
    ce_sol, sim = run(mesh, time, dt, return_comsol=True)

    if not get_test_stats:
        mesh = np.concatenate((sim.mesh, sim.mesh + 1, sim.mesh + 2))
        sol = np.concatenate(
            (ce_sol["anode"](plot_time), ce_sol["separator"](plot_time), ce_sol["cathode"](plot_time)), axis=1
        )
        comsol = np.concatenate(
            (
                sim.comsol_data.ce["anode"](plot_time),
                sim.comsol_data.ce["separator"](plot_time),
                sim.comsol_data.ce["cathode"](plot_time),
            ),
            axis=1,
        )

        utilities.report(mesh, time, sol, comsol, "$c_e$")
        utilities.save_plot(__file__, "plots/compare_ce.png")
        plt.show()

        utilities.report(
            sim.mesh, time, ce_sol["anode"](plot_time), sim.comsol_data.ce["anode"](plot_time), "$c_e^{neg}$"
        )
        utilities.save_plot(__file__, "plots/compare_ce_neg.png")
        plt.show()

        utilities.report(
            sim.mesh, time, ce_sol["separator"](plot_time), sim.comsol_data.ce["separator"](plot_time), "$c_e^{sep}$"
        )
        utilities.save_plot(__file__, "plots/compare_ce_sep.png")
        plt.show()

        utilities.report(
            sim.mesh, time, ce_sol["cathode"](plot_time), sim.comsol_data.ce["cathode"](plot_time), "$c_e^{pos}$"
        )
        utilities.save_plot(__file__, "plots/compare_ce_pos.png")
        plt.show()
    # else:
    #     data = utilities.generate_test_stats(time, comsol, ce_sol, comsol_ce)
    #
    #     return data


if __name__ == "__main__":
    main()
