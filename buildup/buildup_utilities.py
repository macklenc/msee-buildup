import numbers
import os

import dolfin as fem
import munch
import numpy as np
import sympy as sym
from mtnlion import engine
from mtnlion.tools import comsol, loader
from mtnlion.tools.comsol import comsol_preprocessor, collect_parameters, FenicsFunctions
from mtnlion.tools.helpers import set_domain_data
from scipy import interpolate

from mtnlion.domain import eval_domain
from mtnlion.tools.cache import CACHE_DIR


def gather_data(params_sheet, data_source, input_current):
    # Load required cell data
    localdir = os.path.dirname(__file__)
    resources = os.path.join(localdir, "reference/")
    params = engine.fetch_params(os.path.join(resources, params_sheet))
    d_comsol = comsol.load(os.path.join(resources, data_source))
    Uocp_spline = loader.load_numpy_file(os.path.join(resources, "Uocp_spline.npz"))
    input_current = loader.load_csv_file(os.path.join(resources, input_current))
    return d_comsol, params, Uocp_spline, input_current


def set_if_exists(cls, attribute, data):
    if not hasattr(cls, "meta"):
        return data

    cls = cls.meta
    if attribute in cls:
        if cls[attribute] is None or cls[attribute] is False:
            return data
        elif data is not None and data is not False:
            return data
        else:
            return cls[attribute]
    else:
        return data


def equation(test_func=None, trial_func=None, dt=None, boundary=False, lm=None, mu=None):
    def wrap(f):
        d = {
            "boundary": set_if_exists(f, "boundary", boundary),
            "test_func": set_if_exists(f, "test_func", test_func),
            "trial_func": set_if_exists(f, "trial_func", trial_func),
            "time_integration": set_if_exists(f, "time_integration", dt),
            "lm": set_if_exists(f, "lm", lm),
            "mu": set_if_exists(f, "mu", mu),
        }
        if hasattr(f, "meta"):
            f.meta.update(d)
        else:
            f.meta = d

        return f

    return wrap


def kappa_D(R, T, F, t_plus, dfdc, **kwargs):
    kd = 2 * R * T / F * (1 + dfdc) * (t_plus - 1)

    return kd


def param_preprocessor(raw_params):
    params, consts = collect_parameters(raw_params)

    tmpx, soc = sym.symbols("x soc")
    params["Uocp"]["anode"] = params["Uocp"]["anode"][0].subs(tmpx, soc)
    params["Uocp"]["cathode"] = params["Uocp"]["cathode"][0].subs(tmpx, soc)
    params["De_eff"] = consts["De_ref"] * params["eps_e"] ** params["brug_De"]
    params["sigma_eff"] = params["sigma_ref"] * params["eps_s"] ** params["brug_sigma"]
    params["a_s"] = 3 * params["eps_s"] / params["Rs"]
    params["a_s"]["separator"] = 0
    params["cs_0"] = params["csmax"] * params["theta100"]  # set_domain_data(0.325, 0.422)

    consts["F"] = 96487
    consts["T"] = 298.15
    consts["R"] = 8.314
    consts["dfdc"] = 0
    consts["kappa_D"] = kappa_D(**params, **consts)
    consts.kappa_ref = consts.kappa_ref[0]

    return params, consts


@eval_domain("auto", pass_domain=True)
def fenics_constantify(v, name, domain):
    return fem.Constant(v, name="{}[{}]".format(name, domain)) if isinstance(v, numbers.Number) else v


# t.vector()[:] = f[i, fem.dof_to_vertex_map(_V)].astype("double")
def fenics_funcify(function_list, V, t):
    result = []
    for func in function_list:
        new_func = fem.Function(V)
        new_func.vector()[:] = func(t)[fem.dof_to_vertex_map(V)].astype("double")
        result.append(new_func)
    return result


def fenics_domain(placement):
    mesh = fem.IntervalMesh(len(placement) - 1, 0, 3)
    mesh.coordinates()[:] = np.array([placement]).transpose()

    P1 = fem.FiniteElement("CG", mesh.ufl_cell(), 1)
    R0 = fem.FiniteElement("R", mesh.ufl_cell(), 0)

    V = fem.FunctionSpace(mesh, P1)

    class Left(fem.SubDomain):
        def inside(self, x, on_boundary):
            return x[0] < 0.0 + fem.DOLFIN_EPS and on_boundary

    class Right(fem.SubDomain):
        def inside(self, x, on_boundary):
            return x[0] > 1.0 - fem.DOLFIN_EPS and on_boundary

    boundaries = fem.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
    boundaries.set_all(2)

    left = Left()
    right = Right()
    left.mark(boundaries, 0)
    right.mark(boundaries, 1)

    dx = fem.Measure("dx", domain=mesh)
    ds = fem.Measure("ds", domain=mesh, subdomain_data=boundaries)

    return mesh, V, P1, R0, dx, ds


def preprocessor(raw_mesh, params_sheet, data_source, input_current, ic_interp):
    CACHE_DIR.mkdir(parents=True, exist_ok=True)

    # Collect required data
    comsol_data, raw_params, Uocp_spline, input_current = gather_data(params_sheet, data_source, input_current)
    comsol_data = comsol_preprocessor(comsol_data, raw_mesh)

    params, consts = param_preprocessor(raw_params)

    Iapp = interpolate.interp1d(input_current[:, 0], input_current[:, 1], kind=ic_interp)
    Uocp_spline = set_domain_data(Uocp_spline["Uocp_neg"], Uocp_spline["Uocp_pos"])

    raw_sim = munch.Munch(
        comsol_data=munch.Munch(comsol_data),
        params=munch.Munch(params),
        consts=munch.Munch(consts),
        Iapp=Iapp,
        mesh=raw_mesh,
    )

    mesh, V, P1, R0, dx, ds = fenics_domain(raw_mesh)
    fenics_params = munch.Munch({k: fenics_constantify(v, k) for k, v in params.items()})
    fenics_consts = munch.Munch({k: fenics_constantify(v, k) for k, v in consts.items()})
    # fenics_comsol = munch.Munch({k: lambda t: fenics_funcify(v, _V, t) for k, v in comsol_data.items()})
    fenics_sim = munch.Munch(
        comsol_data=FenicsFunctions(comsol_data, V),
        params=fenics_params,
        consts=fenics_consts,
        Iapp=lambda t: float(Iapp(t)),
        mesh=mesh,
        V=V,
        P1=P1,
        R0=R0,
        dx=dx,
        ds=ds,
        Uocp_spline=munch.Munch(Uocp_spline),
    )

    return fenics_sim, raw_sim
