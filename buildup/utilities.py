import os

import dolfin as fem
import matplotlib.pyplot as plt
import numpy as np
import sympy as sym
from mtnlion.tools.helpers import overlay_plt, norm_rmse, EXPRESSIONS
from scipy import interpolate


def create_functions(V, r):
    return tuple(fem.Function(V) for _ in range(r))


def assign_functions(from_funcs, to_funcs, V, i):
    for (f, t) in zip(from_funcs, to_funcs):
        t.vector()[:] = f[i, fem.dof_to_vertex_map(V)].astype("double")


def save_plot(local_module_path, name):
    file = os.path.join(os.path.dirname(local_module_path), name)
    directory = os.path.dirname(os.path.abspath(file))
    if not os.path.exists(directory):
        os.makedirs(directory)

    plt.savefig(name)


def fenics_interpolate(xy_values, cell_type="Lagrange", degree=1):
    x_values = xy_values[:, 0]
    y_values = xy_values[:, 1]

    mesh = fem.IntervalMesh(len(x_values) - 1, 0, 3)  # length doesn't matter
    mesh.coordinates()[:] = np.array([x_values]).transpose()

    V1 = fem.FunctionSpace(mesh, cell_type, degree)
    interp = fem.Function(V1)
    interp.vector()[:] = y_values[fem.vertex_to_dof_map(V1)]

    return interp


def interp_time(time, data):
    y = interpolate.interp1d(time, data, axis=0, fill_value="extrapolate")
    return y


def find_cse_from_cs(comsol):
    data = np.append(comsol.pseudo_mesh, comsol.data.cs.T, axis=1)  # grab cs for each time
    indices = np.where(np.abs(data[:, 1] - 1.0) <= 1e-5)[0]  # find the indices of the solution where r=1 (cse)
    data = data[indices]  # reduce data set to only show cse
    data = data[data[:, 0].argsort()]  # organize the coordinates for monotonicity
    xcoor = data[:, 0]  # x coordinates are in the first column, y should always be 1 now
    neg_ind = np.where(xcoor <= 1)[0]  # using the pseudo dims definition of neg and pos electrodes
    pos_ind = np.where(xcoor >= 1.5)[0]
    cse = data[:, 2:]  # first two columns are the coordinates

    return xcoor, cse.T, neg_ind, pos_ind


# TODO: add builder method for creating expression wrappers
def compose(inner, outer, degree=1):
    return fem.CompiledExpression(
        fem.compile_cpp_code(EXPRESSIONS.composition).Composition(),
        inner=inner.cpp_object(),
        outer=outer.cpp_object(),
        degree=degree,
    )


def piecewise2(V, *values):
    x = sym.Symbol("x[0]")
    E = sym.Piecewise(
        (values[0], x <= 1.0 + fem.DOLFIN_EPS),
        (values[1], sym.And(x > 1.0, x < 2.0)),
        (values[2], x >= 2.0 - fem.DOLFIN_EPS),
        (0, True),
    )
    exp = fem.Expression(sym.printing.ccode(E), degree=0)
    fun = fem.interpolate(exp, V)

    return fun


def mkparam(markers, k_1=fem.Constant(0), k_2=fem.Constant(0), k_3=fem.Constant(0), k_4=fem.Constant(0)):
    var = fem.CompiledExpression(fem.compile_cpp_code(EXPRESSIONS.piecewise).Piecewise(), degree=1)
    var.markers = markers
    # NOTE: .cpp_object() will not be required later as per
    # https://bitbucket.org/fenics-project/dolfin/issues/1041/compiledexpression-cant-be-initialized
    var.k_1, var.k_2, var.k_3, var.k_4 = k_1.cpp_object(), k_2.cpp_object(), k_3.cpp_object(), k_4.cpp_object()
    return var


def report(mesh, time, estimated, true, name):
    rmse = norm_rmse(estimated, true)
    print("{name} normalized RMSE%:".format(name=name))
    for i, t in enumerate(time):
        print("\tt = {time:3.1f}: {rmse:.3%}".format(time=t, rmse=rmse[i]))

    overlay_plt(mesh, time, name, estimated, true)


def picard_solver(F, u, u_, bc, tol=1e-5, maxiter=25):
    eps = 1.0
    iter = 0
    while eps > tol and iter < maxiter:
        iter += 1
        fem.solve(F, u, bc)
        diff = u.vector().get_local() - u_.vector().get_local()
        eps = np.linalg.norm(diff, ord=np.Inf)
        print("iter={}, norm={}".format(iter, eps))
        u_.assign(u)


# Doesn't work!
def newton_solver(F, u_, bc, J, V, a_tol=1e-7, r_tol=1e-10, maxiter=25, relaxation=1):
    eps = 1.0
    iter = 0
    u_inc = fem.Function(V)
    while eps > a_tol and iter < maxiter:
        iter += 1
        # plt.plot(get_1d(u_, _V))
        # plt.show()

        A, b = fem.assemble_system(J, -F, bc)
        fem.solve(A, u_inc.vector(), b)
        eps = np.linalg.norm(u_inc.vector().get_local(), ord=np.Inf)
        print("iter={}, eps={}".format(iter, eps))

        # plt.plot(get_1d(u_inc, _V))
        # plt.show()

        a = fem.assemble(F)
        # for bnd in bc:
        bc.apply(a)
        print("b.norm = {}, linalg norm = {}".format(b.norm("l2"), np.linalg.norm(a.get_local(), ord=2)))
        fnorm = b.norm("l2")

        u_.vector()[:] += relaxation * u_inc.vector()

        print("fnorm: {}".format(fnorm))


def generate_test_stats(time, indices, estimated, true):
    estimated = estimated(time)
    true = true(time)
    rmse = np.array(
        [
            norm_rmse(estimated[:, indices.neg_ind], true[:, indices.neg_ind]),
            norm_rmse(estimated[:, indices.sep_ind], true[:, indices.sep_ind]),
            norm_rmse(estimated[:, indices.pos_ind], true[:, indices.pos_ind]),
        ]
    )

    mean = np.array(
        [
            np.mean(estimated[:, indices.neg_ind]),
            np.mean(estimated[:, indices.sep_ind]),
            np.mean(estimated[:, indices.pos_ind]),
        ]
    )

    std = np.array(
        [
            np.std(estimated[:, indices.neg_ind]),
            np.std(estimated[:, indices.sep_ind]),
            np.std(estimated[:, indices.pos_ind]),
        ]
    )
    return rmse, mean, std
