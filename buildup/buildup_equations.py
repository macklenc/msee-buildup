import dolfin as fem
from mtnlion.domain import eval_domain
from mtnlion.formulas.dfn import SAFE_DICT

from buildup import buildup_utilities, utilities
from buildup.buildup_utilities import equation


@buildup_utilities.equation()
@eval_domain("anode", "cathode")
def U_ocp(cse, csmax, uocp_str, **_):
    """Evaluate the open-circuit potential equation."""
    soc = cse / csmax
    return eval(str(uocp_str), {"__builtins__": None}, {"soc": soc, **SAFE_DICT})


@buildup_utilities.equation()
@eval_domain("physical")
def kappa_ref_f(ce, kappa_ref):
    return eval(str(kappa_ref), {"__builtins__": None}, {"x": ce, **SAFE_DICT})


@eval_domain("anode", "cathode")
def Uocp_interp(Uocp_interp, cse, csmax):
    """Create an interpolator expression for the open circuit potential."""
    eref = utilities.fenics_interpolate(Uocp_interp)

    soc = fem.Expression("cse/csmax", cse=cse, csmax=csmax, degree=1)
    Uocp = utilities.compose(soc, eref)

    return Uocp


@eval_domain("anode", "cathode")
def eta(phis, phie, Uocp, F=0, j=0, Rfilm=0):
    return phis - phie - Uocp - F * j * Rfilm


@eval_domain("physical", pass_domain=True)
def j(ce, cse, eta, csmax, ce0, alpha, k_norm_ref, F, R, T, domain):
    """Flux through the boundary of the solid."""
    if domain == "separator":
        j = fem.Constant(0)
    else:
        j = (
            k_norm_ref
            * (abs(((csmax - cse) / csmax) * ce / ce0) ** (1 - alpha))
            * (abs(cse / csmax) ** alpha)
            * (fem.exp((1 - alpha) * F * eta / (R * T)) - fem.exp(-alpha * F * eta / (R * T)))
        )

    return j


@eval_domain("anode", "cathode")
def phi_s(jbar, phis, v, a_s, F, sigma_eff, L):
    """Charge conservation in the solid."""
    lhs = -sigma_eff / L * fem.dot(fem.grad(phis), fem.grad(v))
    rhs = L * a_s * F * jbar * v

    return rhs - lhs


@eval_domain("physical")
def phi_e(jbar, ce, phie, v, kappa_eff, kappa_Deff, L, a_s, F):
    """Charge conservation in the electrolyte."""
    lhs = kappa_eff / L * fem.dot(fem.grad(phie), fem.grad(v))  # all domains
    rhs1 = -kappa_Deff / L * fem.dot(fem.grad(fem.ln(ce)), fem.grad(v))  # all domains
    rhs2 = L * a_s * F * jbar * v  # electrodes

    return lhs - rhs1 - rhs2


@eval_domain("physical")
def c_e(dt, jbar, ce, v, a_s, De_eff, t_plus, L, eps_e):
    """Concentration of lithium in the electrolyte."""
    lhs = L * eps_e * v  # all domains
    rhs1 = -De_eff / L * fem.dot(fem.grad(ce), fem.grad(v))  # all domains
    rhs2 = L * a_s * (fem.Constant(1) - t_plus) * jbar * v  # electrodes

    return lhs * dt - rhs1 - rhs2


@buildup_utilities.equation()
@eval_domain("auto")
def euler(u, u_1, dt):
    """Create FFL expression for euler explicit/implicit time stepping."""
    if isinstance(u, (list, tuple)):
        if isinstance(u_1, (list, tuple)):
            lhs = [(i - j) / dt for i, j in zip(u, u_1)]
        else:
            raise RuntimeError("Cannot mix iterables with non-iterables")
    else:
        lhs = (u - u_1) / dt

    return lhs


@eval_domain("anode", "cathode")
def c_s(dadt, cs, v, Rs, Ds_ref, legendre):
    """Concentration of lithium in the solid."""
    """time_integration, cs, v are lists of length num_functions"""

    M = legendre.M
    K = legendre.K
    Ns = legendre.num_functions

    lhs_terms = [M[m, n] * dadt[n] * v[m] for m in range(Ns) for n in range(Ns)]
    rhs_terms = [K[m, n] * cs[n] * v[m] for m in range(Ns) for n in range(Ns)]

    lhs = sum(lhs_terms) * Rs
    rhs = sum(rhs_terms) * Ds_ref / Rs

    return rhs + lhs


@eval_domain("anode", "cathode")
def c_s_neumann(jbar, a_m, Rs):
    return jbar * sum(a_m)


# @buildup_utilities.domain("anode")
# def j(ce, cse, phie, phis, Us, csmax, ce0, alpha, k_norm_ref, F, R, T):
#     """Flux through the boundary of the solid."""
#     eta = phis - phie - Us
#     j = (
#         k_norm_ref
#         * (abs(((csmax - cse) / csmax) * ce / ce0) ** (1 - alpha))
#         * (abs(cse / csmax) ** alpha)
#         * (fem.exp((1 - alpha) * F * eta / (R * T)) - fem.exp(-alpha * F * eta / (R * T)))
#     )
#
#     return j
@equation()
@eval_domain("physical")
def kappa_eff_f(kappa_ref, eps_e, brug_kappa):
    kappa_eff = kappa_ref * eps_e ** brug_kappa
    return kappa_eff


@equation()
@eval_domain("physical")
def kappa_Deff_f(kappa_ref, eps_e, kappa_D):
    kappa_Deff = kappa_D * kappa_ref * eps_e
    return kappa_Deff
