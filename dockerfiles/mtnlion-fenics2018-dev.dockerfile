FROM registry.gitlab.com/macklenc/mtnlion/fenics2018:dev as sde
USER mtnlion
WORKDIR /home/mtnlion

# eliminate confusion
RUN cd mtnlion &&\
    python3 setup.py develop --uninstall --user &&\
    cd .. &&\
    rm -rf mtnlion

RUN git clone https://gitlab.com/macklenc/msee-buildup.git --recursive &&\
    cd msee-buildup/mtnlion &&\ 
    python3 setup.py develop --user &&\
    cd .. &&\
    python3 setup.py develop --user &&\
    cd ..

# Install pre-commit hooks
RUN cd msee-buildup/mtnlion &&\
    pre-commit install &&\
    cd .. &&\
    pre-commit install &&\
    cd ..

# Install gvim
RUN sudo apt-get install -y vim-gtk3

# Install sublime
RUN wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add - &&\
    sudo apt-get install -y apt-transport-https &&\
    echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list &&\
    sudo apt-get update &&\
    sudo apt-get install -y sublime-text sublime-merge

# Install pycharm
RUN wget https://download.jetbrains.com/python/pycharm-professional-2019.1.1.tar.gz -qO - | sudo tar xfz - -C /opt/ &&\
    cd /usr/bin &&\
    sudo ln -s /opt/pycharm-*/bin/pycharm.sh pycharm

ENTRYPOINT ["/bin/zsh"]
