#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import find_packages, setup

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()

requirements = ["mtnlion"]

setup_requirements = ["pytest-runner"]

test_requirements = ["pytest"]

setup(
    author="Christopher Macklen",
    author_email="cmacklen@uccs.edu",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.6",
    ],
    description="mtnlion test implementations.",
    entry_points={"console_scripts": ["buildup=buildup.cli:main"]},
    install_requires=requirements,
    license="MIT license",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords="mtnlion buildup",
    name="buildup",
    packages=find_packages(include=["buildup"]),
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/macklenc/msee-buildup",
    version="0.0.1",
    zip_safe=False,
)
